import h5py
import sys
import numpy as np
import argparse


def getSize(size):
    assert (len(size) == 1 or len(size) == 3)
    if len(size) == 1:
        return [size[0]] * 3
    else:
        return size


class DataAverager:
    def __init__(self, size):
        self.size = size
        print(f'Average with {self.size}')

    def getSize(self):
        return self.size

    def __call__(self, dataset: h5py.Dataset):
        for shape, size in zip(dataset.shape, self.size):
            if shape % size != 0:
                print(f'{dataset.name} shape {dataset.shape} incompatible with size {self.size}')
                assert (shape % size == 0)

        reducedSize = []
        for shape, size in zip(dataset.shape, self.size):
            reducedSize.append(int(shape / size))
            reducedSize.append(size)

        data = np.array(dataset).reshape(reducedSize)
        return np.mean(data, axis=(1, 3, 5))


class DoNothing:
    def getSize(self):
        return [1, 1, 1]

    def __call__(self, dataset: h5py.Dataset):
        return dataset


def transferAttributes(srcNode, dstNode, reducer):
    for name, value in srcNode.attrs.items():
        if name == 'dx':
            value *= reducer.getSize()
        if name == 'delta':
            value *= reducer.getSize()
        dstNode.attrs.create(name, value)


def transferGroup(outputFile: h5py.File, inGroup: h5py.Group, reducer):
    print(f'Transfering group {inGroup.name}')
    outGroup = outputFile.create_group(inGroup.name)
    transferAttributes(inGroup, outGroup, reducer)


def transferDataset(outputFile: h5py.File, inDataset: h5py.Dataset, reducer):
    print(f'Transfering dataset {inDataset.name}')
    reducedData = reducer(inDataset)
    outDataset = outputFile.create_dataset(inDataset.name, data=reducedData)
    transferAttributes(inDataset, outDataset, reducer)


def getVisitorFunction(outputFile, reducer=DoNothing()):
    def visitorFunc(name, node):
        if isinstance(node, h5py.Dataset):
            transferDataset(outputFile, node, reducer)
        else:
            transferGroup(outputFile, node, reducer)

    return visitorFunc


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Reduce Cronos output file by averaging cell blocks')
    parser.add_argument('filepath', type=str, help='cronos output file')
    parser.add_argument('size', type=int, nargs='+',
                        help='block size, either one or three arguments')
    parser.add_argument('--output', type=str, help='Reduced output file')
    args = parser.parse_args()

    inputFile = h5py.File(args.filepath, 'r')

    if args.output is None:
        outputFilename = f'{args.filepath}_reduced'
    else:
        outputFilename = args.output
    outputFile = h5py.File(outputFilename, 'w')

    size = getSize(args.size)
    reduction = DataAverager(size)

    visitorFunc = getVisitorFunction(outputFile, reduction)
    inputFile.visititems(visitorFunc)
